from machine import Pin

led = Pin(16, Pin.OUT)
gate_1 = Pin(0, Pin.IN)
gate_2 = Pin(1, Pin.IN)
gate_3 = Pin(2, Pin.IN)
operator_switch = Pin(26, Pin.IN)

def check_your_pins_dear(timer):
    if (operator_switch.value()):
        led.value((not gate_1.value()) and (not gate_2.value()) and (not gate_3.value()))
    else:
        led.value((not gate_1.value()) or (not gate_2.value()) or (not gate_3.value()))

gate_1.irq(check_your_pins_dear, Pin.IRQ_FALLING | Pin.IRQ_RISING)
gate_2.irq(check_your_pins_dear, Pin.IRQ_FALLING | Pin.IRQ_RISING)
gate_3.irq(check_your_pins_dear, Pin.IRQ_FALLING | Pin.IRQ_RISING)
operator_switch.irq(check_your_pins_dear, Pin.IRQ_FALLING | Pin.IRQ_RISING)
